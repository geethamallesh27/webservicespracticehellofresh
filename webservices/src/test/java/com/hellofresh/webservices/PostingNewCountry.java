package com.hellofresh.webservices;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostingNewCountry {

	@SuppressWarnings("unchecked")
	@Test
	public void postingNewCountry(String countryVal,String countryName) {




		RestAssured.baseURI = "http://services.groupkt.com";
		RequestSpecification request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "Test Country"); // Cast
		requestParams.put("alpha2_code", "TC");
		requestParams.put("alpha3_code", "TCY");		
		request.body(requestParams.toJSONString());
		Response response = request.post("/country");
	 
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, "201");
		String successCode = response.jsonPath().get("SuccessCode");
		Assert.assertEquals( "Correct Success code was returned", successCode, "OPERATION_SUCCESS");
	}
	
	
}
