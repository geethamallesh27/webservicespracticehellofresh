package com.hellofresh.webservices;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ValidatingCountryCodes {

	@Test
	public void retrieveAllcountriesValidate() {

		RestAssured.baseURI = "http://services.groupkt.com/country/get";

		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/all");
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
		
		Assert.assertTrue(responseBody.contains("US"));
		Assert.assertTrue(responseBody.contains("DE"));
		Assert.assertTrue(responseBody.contains("GB"));

	}
	
	

}
