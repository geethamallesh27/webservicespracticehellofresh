package com.hellofresh.webservices;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RetrievingCountriesAndValidating {

	 @Test
	public void retrieveAllcountries() {

		RestAssured.baseURI = "http://services.groupkt.com/country/get";

		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/all");
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);

	}

	//@Test(dataProvider="testData")
	public void retrieveSpecificcountries(String countryVal,String countryName) {

		RestAssured.baseURI = "http://services.groupkt.com/country/get";

		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "/iso2code/"+countryVal);
		Assert.assertEquals(response.getStatusCode(),200);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);

		// First get the JsonPath object instance from the Response interface
		JsonPath jsonPathEvaluator = response.jsonPath();
		String city = jsonPathEvaluator.get("RestResponse.result.name");
		String country = jsonPathEvaluator.get("RestResponse.result.alpha2_code");
		// Let us print the city variable to see what we got
		System.out.println("City received from Response " + city);
		System.out.println("Country received from Response " + country);
		// Validate the response
		Assert.assertEquals(city,countryName,"Correct city name received in the Response");
		Assert.assertEquals(country,countryVal, "Correct city code received in the Response");

	}
	
	@DataProvider
	public Object[] testData(){
		
		Object[][] data= new Object[4][2];
		data[0][0]="GB";
		data[0][1]="United Kingdom of Great Britain and Northern Ireland";
		data[1][0]="US";
		data[1][1]="United States of America";
		data[2][0]="DE";
		data[2][1]="Germany";
		//Invalid data
		data[3][0]="ZZ";
		data[3][1]="Dummy";
		
		return data;
		
	}
	
	
	


}
